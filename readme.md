# Expand Tree

Expands a set of assignments to groups to include all ancestors of those groups.


## Example

Element eA is in groups g1 and g2, and element eB is in groups g2 and g3:

* eA --> g1
* eA --> g2
* eB --> g1
* eB --> g3

The groups follow a tree-like structure, with group g3 being nested within group g2.

* g3 --> g2

`ExpandTree` appends the member assignments to also include those ancestor groups, for our example eB is now therefore also in group g2:

* B --> 2

## Usage

`expandtree <tree>`

* `<tree>` - tree file
* `STDIN` - original assignments
* `STDOUT` - new assignments... 
    * ...for convenience, unlike the example above, this will also include the original assignments


## File formats

All files must be in TSV, where each row is:

`<child><TAB><ancestor>`

* `<child>` - the child (child group for the `<tree>`, or the item for `STDIN`/`STDOUT`)
* `<TAB>` - the tab character
* `<ancestor>` - the ancestor (parent group for the `<tree>`, or group for `STDIN`/`STDOUT`)
 
## Example

We have a set of plasmids, annotated with GO annotations, we want to expand this to include the entire GO hierarchy:

`expandtree go.tsv < plasmids.tsv > plasmids-treed.tsv`

Meta-data
---------

```ini
tags= go, tree, tsv, expand
author= Martin Rusilowicz
language= c++
type=application,application-cli
hosts=bitbucket
```
