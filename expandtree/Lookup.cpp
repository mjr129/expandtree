//
// Created by Martin Rusilowicz on 21/08/2017.
//

#include <fstream>
#include <iostream>
#include "Lookup.h"
#include "Node.h"

const Node* Lookup::get( const std::string& id ) const
{
    auto it = this->_table.find(id);
    
    if (it == this->_table.end())
    {
        return nullptr;
    }
    else
    {
        return it->second;
    }
}


Node& Lookup::create( const std::string& id )
{
    auto it = this->_table.find(id);
    
    if (it == this->_table.end())
    {
        Node* result = new Node(id);
        this->_table[id] = result;
        return *result;
    }
    else
    {
        return *it->second;
    }
}


void Lookup::read_file( const std::string& file_name )
{           
    std::cerr << "Reading tree..." << std::endl;

    std::ifstream file_in;
    file_in.open( file_name );
    std::string cell;
    bool        left      = true;
    std::string item;

    while (getline( file_in, cell, left ? static_cast<char>('\t') : static_cast<char>('\n')))
    {
        if (left)
        {
            item = cell;
        }
        else
        {
            Node& child = this->create(item);
            Node& ancestor = this->create(cell);
            child.add_parent(ancestor);
        }

        left = !left;
    }
}


Lookup::~Lookup()
{
    for(const auto& kvp : this->_table)
    {
        delete kvp.second;
    }
}

