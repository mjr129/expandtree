//
// Created by Martin Rusilowicz on 21/08/2017.
//

#include <sstream>
#include <iostream>
#include "Node.h"


void Node::add_parent( const Node& node )
{ 
    _parents.insert( &node);
}


const std::set<const Node*>& Node::get_parents() const
{
    return _parents;
}


Node::Node( const std::string& name )
: _name(name)
, _parents( )
{

}


const std::string& Node::get_name() const
{
    return _name;
}
