//
// Created by Martin Rusilowicz on 21/08/2017.
//

#ifndef EXPANDTREE_NODE_H
#define EXPANDTREE_NODE_H


#include <set>
#include <string>


class Node
{
    private:
        std::set<const Node*> _parents;
        std::string _name;
        
    public:
        Node(const std::string& name);
        void add_parent(const Node& node);
        const std::set<const Node*>& get_parents() const;
        const std::string& get_name() const;
        
};


#endif //EXPANDTREE_NODE_H
