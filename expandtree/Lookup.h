//
// Created by Martin Rusilowicz on 21/08/2017.
//

#ifndef EXPANDTREE_LOOKUP_H
#define EXPANDTREE_LOOKUP_H

#include <string>
#include <map>


class Node;

class Lookup
{
    private:
        std::map<std::string, Node*> _table;
        
    public:
        ~Lookup();
        
        const Node* get(const std::string& id) const;
        Node& create(const std::string& id);
        
        void read_file(const std::string& file_name);
};

#endif //EXPANDTREE_LOOKUP_H
