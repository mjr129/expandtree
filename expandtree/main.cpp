#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include "Lookup.h"
#include "Node.h"


void read_assignments(const Lookup& lookup);

int main(int argc, const char** argv)
{
    if (argc != 2)
    {
        std::cerr << "Invalid arguments. Please consult readme." << std::endl;
        return 1;
    }
    
    std::string in_tree_file_name = argv[1];
    
    // Read tree
    Lookup lookup = Lookup();
    lookup.read_file(in_tree_file_name);
    
    // Read families
    read_assignments(lookup);
    
    // Clean up
    std::cerr << "Cleaning up..." << std::endl;
    return 0;
}

void read_assignments(const Lookup& lookup)
{
    std::cerr << "Reading assignments..." << std::endl;

    std::ifstream file_in;
    std::string cell;
    bool        left      = true;
    std::string item;

    while (getline( std::cin, cell, left ? static_cast<char>('\t') : static_cast<char>('\n')))
    {
        if (left)
        {
            item = cell;
        }
        else
        {   
            const Node* root = lookup.get(cell); 
            
            if (root)
            {
                std::set<const Node*> visited = std::set<const Node*>();
                std::stack<const Node*> nodes = std::stack<const Node*>();
                nodes.push( root );
                
                while(!nodes.empty())
                {
                    const Node& top = *nodes.top();
                    nodes.pop();
                    
                    std::cout << item << "\t" << top.get_name() << std::endl;
                    visited.insert(&top);
                    
                    for (const Node* node : top.get_parents())
                    {
                        std::set<const Node*>::iterator it = visited.find(node);
                        
                        if (it == visited.end())
                        {
                            nodes.push( node );
                        }
                    }
                }
            }
            else
            {
                std::cout << item << "\t" << cell << std::endl;
            }
        }

        left = !left;
    }
}

